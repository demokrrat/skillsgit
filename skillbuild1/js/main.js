
$(document).ready(function ($) {


  let closed_menu = '55px';
  let open_menu = '230px';
  let timer;
  let left_menu = document.getElementById('left-menu');
  let is_open = false;

  left_menu.addEventListener('mouseover', function (e) {
      is_open = true;
      // eslint-disable-next-line func-names
      timer = setTimeout(function () {
          if (is_open) {
              $('.subm-body').each(function () {
                  max_h = $(this)[0].scrollHeight;
                  if (max_h === 0) {
                      $(this).css('max-height', '300px');
                  } else {
                      $(this).css('max-height', max_h);
                  }
              });
              $(left_menu).addClass('open');
              left_menu.style.width = open_menu;
              document.getElementById("content").style.marginLeft = open_menu;
          }
      }, 500);
  }, true);

  left_menu.addEventListener('mouseout', function (e) {
      clearTimeout(timer);
      // eslint-disable-next-line consistent-return
      setTimeout(function () {
          let target_class = e.toElement;
          if ($(target_class).closest('#left-menu').length > 0) {
              return false;
          }
          $(left_menu).removeClass('1open');
          is_open = false;
          $('.subm-body').css('max-height', 0);
          left_menu.style.width = closed_menu;
          document.getElementById("content").style.marginLeft = closed_menu;
      }, 500);
  });

/*  */

$('select.js-dropdown-select').niceSelect();



$('.js-datepicker').each(function (index, element) {
  
  
  let defTime = $(this).data('default-time');

  flatpickr(element, {
    enableTime: true,
    noCalendar: true,
    dateFormat: "H:i",
    time_24hr: true,
    defaultDate: defTime,
    onReady (_, __, fp) {
      fp.calendarContainer.classList.add("timeSelect");
    }
  });
  
});


$('.js-flatpickr').each(function (index, element) {  

  let startDate = $(this).data('start-week');

  let defDate = $(this).data('def-date')

  if ( !defDate  ) {
    defDate = "today"
  } 
  if ( startDate ) {
    RussianLocate.firstDayOfWeek = startDate;
  } else {
    RussianLocate.firstDayOfWeek = 1;
  }
  flatpickr(element, {
    wrap: true,
    locale: RussianLocate,
    
    defaultDate: defDate,
  });
});

$('.js-flatpickr-range').each(function (index, element) {  
  let defDate = $(this).data('def-date')

  if ( !defDate  ) {
    defDate = "today"
  } 

  let startDate = $(this).data('start-week');

  if ( startDate ) {
    RussianLocate.firstDayOfWeek = startDate;
  } else {
    RussianLocate.firstDayOfWeek = 1;
  }

  let fp = flatpickr(element, {
    wrap: true,
    mode: 'range',
    defaultDate: defDate,
    locale: RussianLocate,
  });

});

$('.js-flatpickr-range-week').each(function (index, element) {  
  let defDate = $(this).data('def-date')

  if ( !defDate  ) {
    defDate = "today"
  } 

  let startDate = $(this).data('start-week');

  if ( startDate ) {
    RussianLocate.firstDayOfWeek = startDate;
  } else {
    RussianLocate.firstDayOfWeek = 1;
  }

  let fp = flatpickr(element, {
    wrap: true,
    mode: 'range',
    defaultDate: defDate,
    locale: RussianLocate,
    "plugins": [new weekSelectPlugin({})],
    "onChange": [function(){
        // extract the week number
        // note: "this" is bound to the flatpickr instance
        let weekNumber = this.selectedDates[0]
            ? this.config.getWeek(this.selectedDates[0])
            : null;

        console.log(weekNumber);
    }]
  });

  $(document).on("click","[data-set-date]",function(e) {
      e.preventDefault();
      let date = $(this).data('set-date');
      let $parr = $(this).closest('div');
      fp.setDate(date);
      console.log(date);
      $parr.find("[data-set-date]").removeClass('active');
      $(this).addClass('active')
  });

});

$(document).on("click",".js-show-pass",function() {
    
  if ( $('.js-s-password').is(':hidden') ) {
    $('.js-s-password').show();
    $('.js-h-password').hide();
  } else {
    $('.js-s-password').hide();
    $('.js-h-password').show();
  }

});

$(document).on("click",".js-show-nav",function() {
  console.log(213123);
  $('nav.menu').toggleClass('active');
});


$('.js-show-add-task-popup').click(function (e) { 
  e.preventDefault();
  $('#modalAddTask').modal();
}); 


let containerSelector = '.js-sortable-wrapper';
  let containers = document.querySelectorAll(containerSelector);



  function setIndexNum() {
    $.each($('.js-sortable-wrapper'), function (indexInArray, valueOfElement) { 

      $.each( $(this).find('.js-sortable-item') , function (indexInArray, valueOfElement) { 
          
          // console.log($(this).index());
          $(this).find('.js-sortable-count-number').html(`${indexInArray+1  }.`)
  
      });
  
    });
  }

  setIndexNum();

  $('.accordeon-wrap').click(function (e) { 
    e.preventDefault();
    setIndexNum();
  });


  // eslint-disable-next-line no-unused-vars
  let sortable = new Sortable(containers, {
    draggable: '.js-sortable-item',
    delay: 500,
    mirror: {
      appendTo: containerSelector,
      constrainDimensions: true,
    },
  });


  // element: HTMLElement<any>
  // classesToPrevent: Array<string>
  let isPrevented = (element, classesToPrevent) => {
    let currentElem = element;
    let isParent = false;
    
    while (currentElem) {
      let hasClass = Array.from(currentElem.classList).some((cls) => classesToPrevent.includes(cls));
      if (hasClass) {
        isParent = true;
        currentElem = undefined;
      } else {
        currentElem = currentElem.parentElement;
      }
    }
    
    return isParent;
  }
  
  sortable.on('drag:start', (event) => {
    let currentTarget = event.originalEvent.target;
    
    if (isPrevented(currentTarget, ['div', 'js-dropdown-select'])) {
      event.cancel();
    }
    
    if (isPrevented(currentTarget, ['span', 'select2'])) {
      event.cancel();
    }
  });

  sortable.on('sortable:stop', (e) => {
   

    setTimeout(() => {
      setIndexNum();
    }, 100);
   
  });


  let containerSelector1 = '.js-sortable-blocks-wrapper';
  let containers1 = document.querySelectorAll(containerSelector1);

  // eslint-disable-next-line no-unused-vars
  let swappable1 = new Swappable(containers1, {
    draggable: '.js-sortable-blocks',
    mirror: {
      constrainDimensions: true,
    },
    plugins: [Plugins.ResizeMirror],
  });

  swappable1.on('drag:start', (event) => {
    let currentTarget = event.originalEvent.target;

    if (isPrevented(currentTarget, ['div', 'efficiency-chart__body'])) {
      event.cancel();
    }

    if (isPrevented(currentTarget, ['span', 'js-tooltip'])) {
      event.cancel();
    }
    
    
    if (isPrevented(currentTarget, ['div', 'js-dropdown-select'])) {
      event.cancel();
    }
    
    if (isPrevented(currentTarget, ['span', 'select2'])) {
      event.cancel();
    }
  });


  
if ( $('.js-dropdown-icon').length ) {

  $(document).mouseup(function (e){ // событие клика по веб-документу
    let $div = $(".js-dropdown-icon-wrapper"); // тут указываем ID элемента
    if (!$div.is(e.target) // если клик был не по нашему блоку
        && $div.has(e.target).length === 0 ) { // и не по его дочерним элементам
        $('.js-dropdown-icon-body').removeClass('isActive');
        
        console.log('!active remove without');

    }
  });
  
  $(document).on("click",".js-dropdown-icon",function() {
    let next = $(this).next();
    
    
    $('.js-dropdown-icon-body').not( next ).removeClass('isActive');
  
    if ( !next.is('.isActive') ) {
      next.addClass('isActive');
      console.log('active')
    } else {
      next.removeClass('isActive');
      console.log('!active')
    }
  
    
  });

}

$.each( $('.js-auto-per-view-slider'), function (indexInArray, valueOfElement) { 
  
  let slider = $(this).addClass(`js-auto-per-view-slider${indexInArray}`);
  let scrollBar = $(this).find('.swiper-scrollbar');
  let sliderS = new Swiper(slider, {
    spaceBetween: 40,
    slidesPerView: 'auto',
    freeMode: true,
    scrollbar: {
      el: scrollBar,
      hide: false,
      draggable: true,
      dragSize: 100
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
  });

});

$('.js-tooltip').tooltip();

});


let onContentLoaded = () => {

    let lazyImages = [].slice.call(document.querySelectorAll('img.lazy'));
  
    let lazyLoad = () => {
      lazyImages.forEach((lazyImage) => {
        let topBreakpoint = lazyImage.classList.contains('lazy_last') ?
                              window.innerHeight + 500 : window.innerHeight + 200;
  
        if ((lazyImage.getBoundingClientRect().top <= topBreakpoint
            && lazyImage.getBoundingClientRect().bottom >= 0) && getComputedStyle(lazyImage).display !== 'none') {
          // eslint-disable-next-line
          lazyImage.src = lazyImage.dataset.src;
          // eslint-disable-next-line
          lazyImage.srcset = lazyImage.dataset.srcset;
          lazyImage.classList.remove('lazy');
  
          lazyImages = lazyImages.filter((image) => {
          return image !== lazyImage;
          });
  
          if (lazyImages.length === 0) {
            document.removeEventListener('scroll', lazyLoad);
            window.removeEventListener('resize', lazyLoad);
            window.removeEventListener('orientationchange', lazyLoad);
          }
        }
      });
    };
  
    document.addEventListener('scroll', lazyLoad);
    window.addEventListener('resize', lazyLoad);
    window.addEventListener('orientationchange', lazyLoad);
  }



  

let onLoad = () => {


}
  
  
  
$(DOM.doc).ready(onLoad);
DOM.doc.addEventListener('DOMContentLoaded', onContentLoaded);
  

$(document).ready(function() {

  $(".js-dashboard-tasks__status-toggle").each(function (index, element) {
    // element == this
    let $parrent = $(this).closest('.dashboard-tasks__item');
    if(this.checked) {
      $parrent.addClass('bg-green');
    } else {
      
      $parrent.removeClass('bg-green');
    }
    $(this).change(function() {
        if(this.checked) {
          $parrent.addClass('bg-green');
        } else {
          
          $parrent.removeClass('bg-green');
        }
    });
    
  });
  



  $('.accordeon-wrap  .accordeon-title').click(function() {
    $(this).toggleClass('close');
    $(this).siblings('.accordeon-content').slideToggle();
    return false;
  });	

  $('.nav-row .showAll').click(function() {
    $('.accordeon-wrap').find('.accordeon-content').slideDown();
    $('.accordeon-wrap').find('.accordeon-title').removeClass('close');
    return false;
  });	
  $('.nav-row .hideAll').click(function() {
    $('.accordeon-wrap').find('.accordeon-content').slideUp();
    $('.accordeon-wrap').find('.accordeon-title').addClass('close');
    return false;
  });	


  $('.nav-task-right  .more').click(function(e) {
    e.preventDefault();
    $(this).parents('.nav-task-right').siblings('.nav-task-left').toggleClass('open');
    return false;
  });	

  /*
  $( ".table-wrap .js-sortable-wrapper tr" ).each(function( index ) {
    console.log( `${index  }: ${  $( this ).text()}` ); 
  });

  $('td').click(function(){
    let row_index = $(this).parent().index();
    let col_index = $(this).index();
    console.log(row_index);
 }); */

  $('.table-wrap .js-sortable-wrapper tr').click(function () {
    let index = $(this).siblings('tr').index(this);
    console.log(index);
  });

});



