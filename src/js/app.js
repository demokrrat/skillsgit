/* eslint-disable import/no-unresolved */
/* eslint-disable no-undef */
/* eslint-disable camelcase */
/* eslint-disable func-names */
/* eslint-disable no-unused-vars */
import {DOM} from './_const';

// import './modules/_left-menu';

// dis
// import './modules/_select';

// dis
// import './modules/_dropdownselect';
// import './modules/_datepicker';

import './modules/_charts';
import './modules/_chart.dashboard'
import './modules/_show-pass';
import './modules/_show-menu';
import './modules/_modal';

// dis
import './modules/_sort';

import './modules/_dropdown-icons'
import './modules/_swiperautoperview'
import './modules/_tooltips' 
import './modules/_full-page' 

// dis
// import './modules/_select2'


const onContentLoaded = () => {

    let lazyImages = [].slice.call(document.querySelectorAll('img.lazy'));
  
    const lazyLoad = () => {
      lazyImages.forEach((lazyImage) => {
        const topBreakpoint = lazyImage.classList.contains('lazy_last') ?
                              window.innerHeight + 500 : window.innerHeight + 200;
  
        if ((lazyImage.getBoundingClientRect().top <= topBreakpoint
            && lazyImage.getBoundingClientRect().bottom >= 0) && getComputedStyle(lazyImage).display !== 'none') {
          // eslint-disable-next-line
          lazyImage.src = lazyImage.dataset.src;
          // eslint-disable-next-line
          lazyImage.srcset = lazyImage.dataset.srcset;
          lazyImage.classList.remove('lazy');
  
          lazyImages = lazyImages.filter((image) => {
          return image !== lazyImage;
          });
  
          if (lazyImages.length === 0) {
            DOM.document.removeEventListener('scroll', lazyLoad);
            DOM.window.removeEventListener('resize', lazyLoad);
            DOM.window.removeEventListener('orientationchange', lazyLoad);
          }
        }
      });
    };
  
    DOM.doc.addEventListener('scroll', lazyLoad);
    DOM.win.addEventListener('resize', lazyLoad);
    DOM.win.addEventListener('orientationchange', lazyLoad);
  }



  

const onLoad = () => {


}
  
  
  
$(DOM.doc).ready(onLoad);
DOM.doc.addEventListener('DOMContentLoaded', onContentLoaded);
  

$(document).ready(function() {

  // $(".js-dashboard-tasks__status-toggle").each(function (index, element) {
  //   // element == this
  //   const $parrent = $(this).closest('.dashboard-tasks__item');
  //   if(this.checked) {
  //     $parrent.addClass('bg-green');
  //   } else {
      
  //     $parrent.removeClass('bg-green');
  //   }
  //   $(this).change(function() {
  //       if(this.checked) {
  //         $parrent.addClass('bg-green');
  //       } else {
          
  //         $parrent.removeClass('bg-green');
  //       }
  //   });
    
  // });

  $(document).on("click",".js-dashboard-tasks__status-toggle",function() {

    const $item = $(this).closest('.dashboard-tasks__item');
    $item.toggleClass('dashboard-tasks__item-disabled')

  });
 

  $('.nav-row .showAll').click(function() {
    $('.accordeon-content').find('.table-wrap').slideDown();
    $('.js-toggle-accordions').addClass('is-active');
    return false;
  });	
  $('.nav-row .hideAll').click(function() {
    $('.accordeon-content').find('.table-wrap').slideUp();
    $('.js-toggle-accordions').removeClass('is-active');
    return false;
  });	
  
  

  $(document).on("click",".js-toggle-accordions",function() {
    const $parrent = $(this).closest('.accordeon-title');
    const $elems = $parrent.next('.accordeon-content').find('.table-wrap');
    const $subHeader = $parrent.next('.accordeon-content').find('.table-sub__header .btn-circle');

    if ( $elems.is(':visible') ) {
      $elems.slideUp();
      $subHeader.removeClass('is-active');
      $(this).removeClass('is-active');
    } else {
      $elems.slideDown();
      $subHeader.addClass('is-active');
      $(this).addClass('is-active');
      return false;
    }

    return false;
  });

  $(document).on("click",".js-toogle-current",function() {
    const $parrent = $(this).closest('.table-sub__header');
    const $elems = $parrent.next('.table-wrap');
    const $allArrows = $(this).closest('.accordeon-content').find('.js-toogle-current');
    const $allArrow = $(this).closest('.accordeon-content').prev().find('.toggle-accordion');

    $elems.slideToggle();
    $(this).toggleClass('is-active');

    if ( $(this).is('.is-active') ) {
      $allArrow.addClass('is-active');
    } 

    if ( !$allArrows.is('.is-active') ) {
      $allArrow.removeClass('is-active');
    }

  });
 


  // $('.accordeon-wrap  .accordeon-title')
  // .click(function(){         // вешаем основной обработчик на родителя
  //   $(this).toggleClass('close');
  //   $(this).siblings('.accordeon-content').slideToggle();
  //   return false;
  // })
  // .find('a')
  // .click(function(e){        // вешаем на потомков
  //     e.stopPropagation();   // предотвращаем всплытие
  // });





  $(document).on("click",".nav-task-right  .more",function(e) {
    e.preventDefault();
    $(this).parents('.nav-task-right').siblings('.nav-task-left').toggleClass('open');
    return false;
    
  });




  $('.table-wrap .js-sortable-wrapper tr').click(function () {
    const index = $(this).siblings('tr').index(this);  
  });

  $('[data-toggle="tooltip"]').tooltip()

  $('.subordinates-switch .switch').mouseup(function(e) {
    $('.worker-list').find('.worker-list__item').not('.direct').toggleClass('hide');
  });

});



