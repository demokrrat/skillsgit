/* eslint-disable no-unused-vars */
/* eslint-disable func-names */
import Swiper from 'swiper';


// eslint-disable-next-line func-names
$(document).ready(function () {
  
  // eslint-disable-next-line no-unused-vars
  $.each( $('.js-auto-per-view-slider'), function (indexInArray, valueOfElement) { 
  
    const slider = $(this).addClass(`js-auto-per-view-slider${indexInArray}`);
    const scrollBar = $(this).find('.swiper-scrollbar');
    const sliderS = new Swiper(slider, {
      spaceBetween: 40,
      slidesPerView: 'auto',
      freeMode: true,
      scrollbar: {
        el: scrollBar,
        hide: false,
        draggable: true,
        dragSize: 100
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
    });
  
  });

});

  