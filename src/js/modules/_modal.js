/* eslint-disable func-names */
/* eslint-disable import/no-unresolved */
import 'bootstrap/js/dist/modal';



$(document).on("click",".js-show-add-task-popup",function(e) {
  e.preventDefault();
  $('#modalAddTask').modal();
});

$(document).on("click",".s-show-delete-task-popup",function(e) {
  e.preventDefault();
  $('#modalDeleteTask').modal();
});

$(document).on("click",".js-show-functions-popup",function(e) {
  e.preventDefault();
  $('#modalFunktions').modal();
});
 

