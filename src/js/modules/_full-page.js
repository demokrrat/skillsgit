/* eslint-disable no-undef */
/* eslint-disable no-unused-vars */
/* eslint-disable func-names */
import Swiper from 'swiper';


// eslint-disable-next-line func-names
$(document).ready(function () {
  
  // eslint-disable-next-line no-unused-vars
    const sliderF = new Swiper('.js-full-page', {
      spaceBetween: 40,
      slidesPerView: 1,
      direction: 'vertical',
      autoHeight: true,
      on: {
        init () {
          $('body').addClass('hidden-o');
          console.log(1)
        },
      }
    });

    $(document).on("click",".js-go-to-2",function() {

      sliderF.slideNext();
  
    });

    $(document).on("click",".js-go-to-1",function() {

      sliderF.slidePrev();
  
    });


});

  