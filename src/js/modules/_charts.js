/* eslint-disable vars-on-top */
/* eslint-disable no-var */
/* eslint-disable no-constant-condition */
/* eslint-disable block-scoped-let */
/* eslint-disable no-param-reassign */
/* eslint-disable camelcase */
/* eslint-disable prefer-rest-params */
/* eslint-disable no-use-before-define */
/* eslint-disable no-console */
/* eslint-disable no-underscore-dangle */
/* eslint-disable no-shadow */
/* eslint-disable func-names */
/* eslint-disable object-shorthand */
/* eslint-disable no-unused-vars */

import Chart from 'chart.js';
import * as ChartAnnotation from 'chartjs-plugin-annotation';
import 'bootstrap/js/dist/modal';


function addData(chart, label, data) {
  chart.data.labels.push(label);
  chart.data.datasets.forEach((dataset) => {
      dataset.data.push(data);
  });
  chart.update();
}
function removeData(chart) {
  chart.data.labels.pop();
  chart.data.datasets.forEach((dataset) => {
      dataset.data.pop();
  });
  chart.update();
}



if ( $('.js-line-modal').length ) {



  // eslint-disable-next-line prefer-const
  let ctxModal = document.getElementById('chartStatsModal').getContext('2d');
  // eslint-disable-next-line prefer-const
  let lineModal = new Chart(ctxModal, {
    responsive: true,
    
    bezierCurve: false,
    type: 'line',
    data: {
      labels: '',
      datasets: '',
    },
    options: {
      maintainAspectRatio: false,
        responsive: true, 
      elements: {
        line: {
            tension: 0
            },
        
      },
      legend: {
        display: false,
      },
      hover: {
        mode: 'point',
      },
      scales: {
        
        xAxes: [{
            
          gridLines: {
            display: true,
					drawBorder: true,
					drawOnChartArea: false,
          },
        }
      ],
        yAxes: [{
            gridLines: {
              display: true,
              drawBorder: true,
              drawOnChartArea: false,
                
            }
            
        }
      ]
    }
    }, 
  
  })

  $(document).on('click', '.js-show-chart-full-popup', function (e) {
    e.preventDefault();

    const parrent = $(this).closest('.efficiency-chart');
    const canvas = parrent.find('canvas');
    const labels1 =  canvas.data('labels');
    const datasets1 = canvas.data('datasets');

    console.log(labels1)

    console.log(datasets1)

    
  

   


    $('#modalShowChart').modal().on('shown.bs.modal', function (e) {
      // do something....

      lineModal.reset();
      lineModal.destroy();
      ctxModal = document.getElementById('chartStatsModal').getContext('2d');
      lineModal = new Chart(ctxModal, {
        responsive: true,
        
        bezierCurve: false,
        type: 'line',
        data: {
          labels: labels1,
          datasets: datasets1,
        },
        options: {
          maintainAspectRatio: false,
            responsive: true, 
          elements: {
            line: {
                tension: 0
                },
            
          },
          legend: {
            display: false,
          },
          hover: {
            mode: 'point',
          },
          scales: {
        
            xAxes: [{
                
              gridLines: {
                display: true,
              drawBorder: true,
              drawOnChartArea: false,
              },
            }
          ],
            yAxes: [{
                gridLines: {
                  display: true,
                  drawBorder: true,
                  drawOnChartArea: false,
                    
                }
                
            }
          ]
        }
        }, 
      
      })

    


    })
});

}

const customTooltips = function(context) {
  const {chart} = context;
  $(chart.canvas).css('cursor', 'pointer');

  const positionY = chart.canvas.offsetTop;
  const positionX = chart.canvas.offsetLeft;

  $('.chartjs-tooltip').css({
    opacity: 0,
  });

  const {tooltip} = context;
  if (!tooltip || !tooltip.opacity) {
    return;
  }

  if (tooltip.dataPoints.length > 0) {
    tooltip.dataPoints.forEach(function(dataPoint) {
      const content = [dataPoint.label, dataPoint.formattedValue].join(': ');
      const $tooltip = $(`#tooltip-${  dataPoint.datasetIndex}`);
      const pos = dataPoint.element.tooltipPosition();

      $tooltip.html(content);
      $tooltip.css({
        opacity: 1,
        top: `${positionY + pos.y  }px`,
        left: `${positionX + pos.x  }px`,
      });
    });
  }
};

// Chart.plugins.register({
//   beforeDatasetsDraw: function(chart) {
//      if (chart.tooltip._active && chart.tooltip._active.length) {
//         const activePoint = chart.tooltip._active[0];
//            const {ctx} = chart;
//            const y_axis = chart.scales['y-axis-0'];
//            const {x} = activePoint.tooltipPosition();
//            const topY = y_axis.top;
//            const bottomY = y_axis.bottom;
//         // draw line
//         ctx.save();
//         ctx.beginPath();
//         ctx.moveTo(x, topY);
//         ctx.lineTo(x, bottomY);
//         ctx.lineWidth = 1;
//         ctx.strokeStyle = '#CADCFE';
//         ctx.stroke();
//         ctx.restore();
//      }
//   }
// });

$('.js-line').each(function (index, element) {
  // element == this
  const labels = $(this).data('labels');
  const datasets = $(this).data('datasets');
  
  const ctx = element.getContext('2d');




 
  const line = new Chart(ctx, {
    responsive: true,
    
    bezierCurve: false,
    type: 'line',
    data: {
      labels: labels,
      datasets: datasets,
      
    
    },
    
    options: {
      maintainAspectRatio: false,
        responsive: true, 
      elements: {
        line: {
            tension: 0
            },
        
      },
      legend: {
        display: false,
      },
      hover: {
        mode: 'point',
      },
      tooltips: {
        // enabled: false,
        intersect: false,
        mode: 'y'
        // custom: customTooltips
      },
      scales: {
        
        xAxes: [{
          ticks: {
            beginAtZero: true
          },
          gridLines: {
            display: true,
            drawBorder: true,
            drawOnChartArea: true,
          },
        }
      ],
        yAxes: [{
            gridLines: {
              display: true,
              drawBorder: true,
              drawOnChartArea: false,
                
            }
            
        }
      ]
    }
    },
   
  })

  


  
});


$('.js-bar-stacked').each(function (index, element) {
  // element == this
 // const yLegend = element.data('ylegend');
 const labels = $(this).data('labels');
 const datasets = $(this).data('datasets');
 const lineChart = $(this).data('line-chart');
  const ctx = element.getContext('2d');




  const scatterChart = new Chart(ctx, {
     // Instruct chart js to respond nicely.
    responsive: true,
    type: 'bar',
    data: {
      labels: labels,
      datasets: datasets,
      
    
    },
    tooltips: {
      // callbacks: {
      //   title: function(tooltipItems, data) {
      //     return `${tooltipItems.xLabel  } ч`;
      //   },
      //   label: function(tooltipItems, data) { 
      //     return `${tooltipItems.yLabel  } ч`;
      //   }
      // }
  },
  options: {
    legend: {
      display: false,
    },
    responsive: true,
    maintainAspectRatio: false,
      scales: {
        xAxes: [{
            stacked: true,
            
            gridLines: {
                display: false,
                color: "#E1E4E9"
            },
            ticks: {
                beginAtZero: false,
                fontColor: "#878a92",
                fontSize: 12,
                padding: 0,
            }
        }
      ],
        yAxes: [{
          stacked: true,
          tickLength: 1,
            gridLines: {
                // drawBorder: false,
                display: true,
                color: "#E1E4E9",
                zeroLineColor: '#ffcc33'
                
            },
            ticks: {
                fontColor: "#878a92",
                fontSize: 12,
                stepSize: 1,
                callback: function(value, index, values) {
                    
                    return `${value  }ч`;
                    
                }
            },
            type: 'linear',
            
        }
      ]
    },
    annotation: {
      events: ["click"],
        annotations: [
          // {
          //   drawTime: "beforeDatasetsDraw",
          //   id: "hline",
          //   type: "box",
           
          //   // ID of the X scale to bind onto
          //   xScaleID: 'x-axis-0',

          //   // ID of the Y scale to bind onto
          //   yScaleID: 'y-axis-0',

          //   // Left edge of the box. in units along the x axis
          //   xMin: -1,

          //   // Right edge of the box
          //   xMax: 10,

          //   // Top edge of the box in units along the y axis
          //   yMax: 9.1,

          //   // Bottom edge of the box
          //   yMin:  8.9,


          //   // Fill color
          //   backgroundColor: fillPattern,
          //   // label: {
          //   //   content: "План",
          //   //   enabled: true,
          //   //   position: "right",
          //   //   xAdjust: -10,
          //   //   backgroundColor: 'rgba(0,0,0,0)',
          //   //     fontFamily: "Proxima Nova,-apple-system,sans-serif",
          //   //     fontSize: 12,
          //   //     fontStyle: "normal",
          //   //     fontColor: "#202020",
          //   // },
          //   onClick: function(e) {
          //     // The annotation is is bound to the `this` variable
          //     console.log("Annotation", e.type, this._view);
          //   }
          // },
          {
            drawTime: "afterDraw",
            id: "hline0",
            type: "line",
            mode: "horizontal",
            scaleID: "y-axis-0",
            value: lineChart,
            endValue: lineChart,
            borderColor: "#C0CFE4",
            borderWidth: 3,
            // label: {
            //   content: "План",
            //   enabled: true,
            //   position: "right",
            //   xAdjust: -10,
            //   backgroundColor: 'rgba(0,0,0,0)',
            //     fontFamily: "Proxima Nova,-apple-system,sans-serif",
            //     fontSize: 12,
            //     fontStyle: "normal",
            //     fontColor: "#202020",
            // },
            onClick: function(e) {
              // The annotation is is bound to the `this` variable
              console.log("Annotation", e.type, this._view);
            }
          },
        
          {
            drawTime: "beforeDatasetsDraw",
            id: "hline1",
            type: "line",
            mode: "horizontal",
            scaleID: "y-axis-0",
            value: lineChart+2,
            borderColor: "#fff",
            borderWidth: 0,
            endValue: lineChart+2,
            // label: {
            //   content: "План",
            //   enabled: true,
            //   position: "right",
            //   xAdjust: -10,
            //   backgroundColor: 'rgba(0,0,0,0)',
            //     fontFamily: "Proxima Nova,-apple-system,sans-serif",
            //     fontSize: 12,
            //     fontStyle: "normal",
            //     fontColor: "#202020",
            // },
            onClick: function(e) {
              // The annotation is is bound to the `this` variable
              console.log("Annotation", e.type, this._view);
            }
          },
        ]
    },

    },
    
  });
  
});



// const chart    = document.getElementById('myChart-user-green').getContext('2d');
//     const gradient = chart.createLinearGradient(0, 0, 0, 450);

// gradient.addColorStop(0, 'rgba(53, 255,53, 0.7)');
// gradient.addColorStop(0.5, 'rgba(255, 255,255, 0.25)');
// gradient.addColorStop(1, 'rgba(0, 226, 53, 0.1)');


// const data  = {
//     labels: [ '05.02', '06.02', '07.02', '08.02', '09.02', '10.02', '11.02', '12.02', '13.02', '14.02', '15.02', '16.02',],
//     datasets: [{
// 			label: '',
// 			backgroundColor: gradient,
// 			pointBackgroundColor: 'white',
// 			borderWidth: 1,
// 			borderColor: '#12ff49',
// 			data: [30, 80, 60, 15, 54, 77, 80, 40, 99, 54, 24]
//     }]
// };


// const options = {
// 	responsive: true,
// 	maintainAspectRatio: true,
// 	animation: {
// 		easing: 'easeInOutQuad',
// 		duration: 520
// 	},
// 	scales: {
// 		xAxes: [{
// 			gridLines: {
// 				color: 'rgba(200, 200, 200, 0.05)',
// 				lineWidth: 1
// 			}
// 		}],
// 		yAxes: [{
// 			gridLines: {
// 				color: 'rgba(200, 200, 200, 0.08)',
// 				lineWidth: 1
// 			}
// 		}]
// 	},
// 	elements: {
// 		line: {
// 			tension: 0.4
// 		}
// 	},
// 	legend: {
// 		display: false
// 	},
// 	point: {
// 		backgroundColor: 'green'
// 	},
// 	tooltips: {
// 		titleFontFamily: 'Open Sans',
// 		backgroundColor: 'rgba(0,0,0,0.3)',
// 		titleFontColor: 'red',
// 		caretSize: 5,
// 		cornerRadius: 2,
// 		xPadding: 10,
// 		yPadding: 10
// 	}
// };


// const chartInstance = new Chart(chart, {
//     type: 'line',
//     data: data,
// 		options: options
// });


// // progress chart
// $('.js-progress__chart').each(function (index, element) {
//   // element == this

//     // round corners
// 	Chart.pluginService.register({
// 		afterUpdate: function (chart) {
// 			if (chart.config.options.elements.arc.roundedCornersFor !== undefined) {
// 				const arc = chart.getDatasetMeta(0).data[chart.config.options.elements.arc.roundedCornersFor];
// 				arc.round = {
// 					x: (chart.chartArea.left + chart.chartArea.right) / 2,
// 					y: (chart.chartArea.top + chart.chartArea.bottom) / 2,
// 					radius: (chart.outerRadius + chart.innerRadius) / 2,
// 					thickness: (chart.outerRadius - chart.innerRadius) / 2 - 1,
// 					backgroundColor: arc._model.backgroundColor
// 				}
// 			}
// 		},

// 		afterDraw: function (chart) {
// 			if (chart.config.options.elements.arc.roundedCornersFor !== undefined) {
// 				const {ctx} = chart.chart;
// 				const arc = chart.getDatasetMeta(0).data[chart.config.options.elements.arc.roundedCornersFor];
// 				const startAngle = Math.PI / 2 - arc._view.startAngle;
// 				const endAngle = Math.PI / 2 - arc._view.endAngle;

// 				ctx.save();
// 				ctx.translate(arc.round.x, arc.round.y);
// 				// console.log(arc.round.startAngle)
// 				ctx.fillStyle = arc.round.backgroundColor;
// 				ctx.beginPath();
// 				ctx.arc(arc.round.radius * Math.sin(startAngle), arc.round.radius * Math.cos(startAngle), arc.round.thickness, 0, 2 * Math.PI);
// 				ctx.arc(arc.round.radius * Math.sin(endAngle), arc.round.radius * Math.cos(endAngle), arc.round.thickness, 0, 2 * Math.PI);
// 				ctx.closePath();
// 				ctx.fill();
// 				ctx.restore();
// 			}
// 		},
// 	});

// 	// write text plugin
// 	Chart.pluginService.register({
// 		afterUpdate: function (chart) {
// 			if (chart.config.options.elements.center) {
// 				const {helpers} = Chart;
// 				const centerConfig = chart.config.options.elements.center;
// 				const globalConfig = Chart.defaults.global;
// 				const {ctx} = chart.chart;

// 				const fontStyle = helpers.getValueOrDefault(centerConfig.fontStyle, globalConfig.defaultFontStyle);
// 				const fontFamily = helpers.getValueOrDefault(centerConfig.fontFamily, globalConfig.defaultFontFamily);

// 				if (centerConfig.fontSize)
// 					var {fontSize} = centerConfig;
// 					// figure out the best font size, if one is not specified
// 				else {
// 					ctx.save();
// 					let fontSize = helpers.getValueOrDefault(centerConfig.minFontSize, 1);
// 					const maxFontSize = helpers.getValueOrDefault(centerConfig.maxFontSize, 256);
// 					const maxText = helpers.getValueOrDefault(centerConfig.maxText, centerConfig.text);

// 					do {
// 						ctx.font = helpers.fontString(fontSize, fontStyle, fontFamily);
// 						const textWidth = ctx.measureText(maxText).width;

// 						// check if it fits, is within configured limits and that we are not simply toggling back and forth
// 						if (textWidth < chart.innerRadius * 2 && fontSize < maxFontSize)
// 							fontSize += 1;
// 						else {
// 							// reverse last step
// 							fontSize -= 1;
// 							break;
// 						}
// 					} while (true)
// 					ctx.restore();
// 				}

// 				// save properties
// 				chart.center = {
// 					font: helpers.fontString(fontSize, fontStyle, fontFamily),
// 					fillStyle: helpers.getValueOrDefault(centerConfig.fontColor, globalConfig.defaultFontColor)
// 				};
// 			}
// 		},
// 		afterDraw: function (chart) {
// 			if (chart.center) {
// 				const centerConfig = chart.config.options.elements.center;
// 				const {ctx} = chart.chart;

// 				ctx.save();
// 				ctx.font = chart.center.font;
// 				ctx.fillStyle = chart.center.fillStyle;
// 				ctx.textAlign = 'center';
// 				ctx.textBaseline = 'middle';
// 				const centerX = (chart.chartArea.left + chart.chartArea.right) / 2;
// 				const centerY = (chart.chartArea.top + chart.chartArea.bottom) / 2;
// 				ctx.fillText(centerConfig.text, centerX, centerY);
// 				ctx.restore();
// 			}
// 		},
// 	})


//   const firstval = $(this).data('val');
//   const secondval = 100 - firstval;

// 	const config = {
//     type: 'doughnut',
// 		data: {
// 			datasets: [{
// 				data: [firstval, secondval],
// 				backgroundColor: [
// 					"#0353A4",
// 					"#CFDCEB"
// 				],
// 				hoverBackgroundColor: [
// 					"#0353A4",
// 					"#CFDCEB"
// 				]
// 			}]
// 		},
// 		options: {
      
//       responsive: true, 
//       maintainAspectRatio: false,
//       width: 62,
//       height: 62,
//       aspectRatio: 1,
//       // maintainAspectRatio: false,
//       cutoutPercentage: 80,
//       tooltips: {
//         enabled: false
//       },
//       layout: {
//         padding: {
//             left: 0,
//             right: 0,
//             top: 0,
//             bottom: 0
//         }
//     },
// 			elements: {
// 				arc: {
// 					roundedCornersFor: 0
// 				},
// 				// center: {
// 				// 	// the longest text that could appear in the center
// 				// 	maxText: '100%',
// 				// 	text: `${firstval}<span>%</span>`,
// 				// 	fontColor: '#0353A4',
// 				// 	fontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",
// 				// 	fontStyle: 'normal',
// 				// 	fontSize: 15,
// 				// 	// if a fontSize is NOT specified, we will scale (within the below limits) maxText to take up the maximum space in the center
// 				// 	// if these are not specified either, we default to 1 and 256
// 				// 	minFontSize: 1,
// 				// 	maxFontSize: 256,
// 				// }
// 			}
// 		}
// 	};


// 		const ctx = element.getContext("2d");
// 		const myChart = new Chart(ctx, config);

  
// });
