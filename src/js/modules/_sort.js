/* eslint-disable eqeqeq */
/* eslint-disable no-undef */
/* eslint-disable no-console */
  /* eslint-disable no-shadow */
  /* eslint-disable no-unused-vars */
  /* eslint-disable func-names */
  /* eslint-disable no-new */
import {Sortable, Swappable, Plugins, Draggable} from '@shopify/draggable';

  const containerSelector = '.js-sortable-wrapper';
  const containers = document.querySelectorAll(containerSelector);



  function setIndexNum(that) {
    const arraynumber = []
    $.each($('.js-sortable-wrapper'), function (indexInArray, valueOfElement) { 

      const table = indexInArray
      
   

      $.each( $(this).find('.bordered-top') , function (indexInArray, valueOfElement) { 

         
          
          // console.log($(this).index());
          $(this).find('.js-sortable-count-number').html(`${indexInArray+1  }.`)
         
      });
  
    });
  }

  function setIndexNum1(that) {
      
      // console.log(that)

      const arrayelem = []

      // console.log(15546)
      $.each( $(that).find('.bordered-top') , function (indexInArray, valueOfElement) { 


          const id = $(this).data('id');
          const model = $(this).data('model')
         
          
          // console.log($(this).index());
          $(this).find('.js-sortable-count-number').html(`${indexInArray+1  }.`)

          arrayelem.push({index: indexInArray, idelem: id, tablemodel: model})

          // eslint-disable-next-line no-undef
          // arrayelem.index = indexInArray
          // arrayelem.idelem = id
          // arrayelem.tablemodel = model


          



         
      });

      // console.log(arrayelem)
      return arrayelem
  
  }

  setIndexNum();

  $('.accordeon-wrap').click(function () { 
    // e.preventDefault();
    setIndexNum();
  });

  const wallClass = 'CollidableWall';
  const walls = document.querySelectorAll(`.${wallClass}`);


  // eslint-disable-next-line no-unused-vars
  const sortable = new Sortable(containers, {
    draggable: '.js-sortable-item',
    delay: 500,
    // mirror: {
    //   appendTo: containerSelector,
    //   constrainDimensions: true,
    // },
    collidables: '.CollidableObstacle',
    plugins: [Plugins.Collidable],

  });

  sortable.removePlugin(Draggable.Plugins.AutoScroll);

  // console.log('new js')

  // element: HTMLElement<any>
  // classesToPrevent: Array<string>
  const isPrevented = (element, classesToPrevent) => {
    let currentElem = element;
    let isParent = false;
    
    while (currentElem) {
      const hasClass = Array.from(currentElem.classList).some((cls) => classesToPrevent.includes(cls));
      if (hasClass) {
        isParent = true;
        currentElem = undefined;
      } else {
        currentElem = currentElem.parentElement;
      }
    }
    
    return isParent;
  }
  
  sortable.on('drag:start', (event) => {
    const currentTarget = event.originalEvent.target;
    
    if (isPrevented(currentTarget, ['div', 'js-dropdown-select'])) {
      event.cancel();
    }

    if (isPrevented(currentTarget, ['div', 'js-disabled-sort'])) {
      event.cancel();
    }

    if (isPrevented(currentTarget, ['input', 'input-checkbox'])) {
      event.cancel();
    }
    
    if (isPrevented(currentTarget, ['span', 'select2'])) {
      event.cancel();
    }

    if (isPrevented(currentTarget, ['textarea', 'form-control'])) {
      event.cancel();
    }
    if (isPrevented(currentTarget, ['input', ''])) {
      event.cancel();
    }
  });

  sortable.on('sortable:stop', (e) => {
   
    console.log(e.newContainer);
    console.log(e.newIndex);

    // тут аякс

   

    setTimeout(() => {
      setIndexNum1(e.newContainer);
      const datae = JSON.stringify(setIndexNum1(e.newContainer));
      const csrfParam = $('meta[name="csrf-param"]').attr("content");
      const csrfToken = $('meta[name="csrf-token"]').attr("content");
      $.ajax({
        url: "/cabinet/api/sortable",
        type: "POST",
        dataType: 'json',
        data: datae,
    }).done(function (data) {
        if(data.error == true){
            showErrorMessage(data.msg);
        }else{
            showSuccessMessage(data.msg);
        }

    });

    console.log(datae)
    }, 100);
   
  });

  // --- Draggable events --- //
  // sortable.on('collidable:in', ({collidingElement}) => {
  //   if (collidingElement.classList.contains(wallClass)) {
  //     walls.forEach((wall) => wall.classList.add('isColliding'));
  //   } else {
  //     collidingElement.classList.add('isColliding');
  //   }
  // });

  // sortable.on('collidable:out', ({collidingElement}) => {
  //   if (collidingElement.classList.contains(wallClass)) {
  //     walls.forEach((wall) => wall.classList.remove('isColliding'));
  //   } else {
  //     collidingElement.classList.remove('isColliding');
  //   }
  // });


  const containerSelector1 = '.js-sortable-blocks-wrapper';
  const containers1 = document.querySelectorAll(containerSelector1);

  // eslint-disable-next-line no-unused-vars
  const swappable1 = new Swappable(containers1, {
    draggable: '.js-sortable-blocks',
    mirror: {
      constrainDimensions: true,
    },
    plugins: [Plugins.ResizeMirror],
  });

  swappable1.on('drag:start', (event) => {
    const currentTarget = event.originalEvent.target;

    if (isPrevented(currentTarget, ['div', 'efficiency-chart__body'])) {
      event.cancel();
    }

    if (isPrevented(currentTarget, ['span', 'js-tooltip'])) {
      event.cancel();
    }
    
    
    if (isPrevented(currentTarget, ['div', 'js-dropdown-select'])) {
      event.cancel();
    }
    
    if (isPrevented(currentTarget, ['span', 'select2'])) {
      event.cancel();
    }

    if (isPrevented(currentTarget, ['textarea', 'form-control'])) {
      event.cancel();
    }
    if (isPrevented(currentTarget, ['input', ''])) {
      event.cancel();
    }
  });
