/* eslint-disable prefer-const */
/* eslint-disable no-unused-vars */
/* eslint-disable no-console */
/* eslint-disable no-undef */
/* eslint-disable new-cap */
/* eslint-disable func-names */
import flatpickr from "flatpickr";
// eslint-disable-next-line import/extensions
import { Russian as RussianLocate } from "../../../node_modules/flatpickr/dist/l10n/ru";



import weekSelectPlugin from "../../../node_modules/flatpickr/dist/plugins/weekSelect/weekSelect"
// import weekSelectPlugin from '../lib/weekSelect';

$('.js-datepicker-def').each(function (index, element) {
  
  
  const defTime = $(this).data('default-time');

  flatpickr(element, {
    time_24hr: true,
    defaultDate: defTime,
    onReady (_, __, fp) {
      // fp.calendarContainer.classList.add("timeSelect");
    }
  });
  
});

$('.js-datepicker').each(function (index, element) {
  
  
  const defTime = $(this).data('default-time');

  flatpickr(element, {
    enableTime: true,
    noCalendar: true,
    dateFormat: "H:i",
    time_24hr: true,
    defaultDate: defTime,
    onReady (_, __, fp) {
      fp.calendarContainer.classList.add("timeSelect");
    }
  });
  
});


$('.js-flatpickr').each(function (index, element) {  

  const startDate = $(this).data('start-week');

  let defDate = $(this).data('def-date')

  if ( !defDate  ) {
    defDate = "today"
  } 
  if ( startDate ) {
    RussianLocate.firstDayOfWeek = startDate;
  } else {
    RussianLocate.firstDayOfWeek = 1;
  }
  flatpickr(element, {
    wrap: true,
    locale: RussianLocate,
    
    defaultDate: defDate,
  });
});

$('.js-flatpickr-range').each(function (index, element) {  
  let defDate = $(this).data('def-date')

  if ( !defDate  ) {
    defDate = "today"
  } 

  const startDate = $(this).data('start-week');

  if ( startDate ) {
    RussianLocate.firstDayOfWeek = startDate;
  } else {
    RussianLocate.firstDayOfWeek = 1;
  }

  const fp = flatpickr(element, {
    wrap: true,
    mode: 'range',
    defaultDate: defDate,
    locale: RussianLocate,
  });

});

$('.js-flatpickr-range-week').each(function (index, element) {  
  let defDate = $(this).data('def-date')

  if ( !defDate  ) {
    defDate = "today"
  } 

  const startDate = $(this).data('start-week');

  if ( startDate ) {
    RussianLocate.firstDayOfWeek = startDate;
  } else {
    RussianLocate.firstDayOfWeek = 1;
  }

  const fp = flatpickr(element, {
    wrap: true,
    mode: 'range',
    defaultDate: defDate,
    locale: RussianLocate,
    "plugins": [new weekSelectPlugin({})],
    "onChange": [function(){
        // extract the week number
        // note: "this" is bound to the flatpickr instance
        const weekNumber = this.selectedDates[0]
            ? this.config.getWeek(this.selectedDates[0])
            : null;

        console.log(weekNumber);
    }]
  });

  $(document).on("click","[data-set-date]",function(e) {
      e.preventDefault();
      const date = $(this).data('set-date');
      const $parr = $(this).closest('div');
      fp.setDate(date);
      console.log(date);
      $parr.find("[data-set-date]").removeClass('active');
      $(this).addClass('active')
  });

});
