/* eslint-disable no-console */
/* eslint-disable no-unused-vars */
/* eslint-disable func-names */
import select2 from 'select2';

$.each($('.form-def__select-search'), function (indexInArray, valueOfElement) { 
  const placeholderval = $(this).data('placeholder') || '';
  $(this).select2({
      // dropdownCssClass: 'sort-list',
      "language": {
          "noResults": function() {
              return "Ничего не найдено"
          }
      },
      placeholder: placeholderval
  });
});
