/* eslint-disable func-names */
$(document).ready(function () {
  $(document).on("click",".js-show-pass",function() {
    
    if ( $('.js-s-password').is(':hidden') ) {
      $('.js-s-password').show();
      $('.js-h-password').hide();
    } else {
      $('.js-s-password').hide();
      $('.js-h-password').show();
    }

  });

});