/* eslint-disable object-shorthand */
/* eslint-disable no-unused-vars */
/* eslint-disable func-names */
import Chart from 'chart.js';



$('.chartGreen').each(function (index, element) {
  // element == this
  const labels = $(this).data('labels');
  const datasets = $(this).data('datasets');
  
  const ctx = element.getContext('2d');




 
  const line = new Chart(ctx, {
    responsive: true,
    
    bezierCurve: false,
    type: 'line',
    data: {
      labels: labels,
      datasets: datasets,
      
    
    },
    
    options: {
      maintainAspectRatio: false,
        responsive: true, 
      elements: {
        line: {
            tension: 0
            },
        
      },
      legend: {
        display: false,
      },
      hover: {
        mode: 'point',
      },
      tooltips: {
        enabled: false,
        intersect: false,
        mode: 'y'
        // custom: customTooltips
      },
      scales: {
        
        xAxes: [{
          ticks: {
            display: false
        },
          gridLines: {
            display: false,
            drawBorder: false,
            drawOnChartArea: false,
          },
        }
      ],
        yAxes: [{
          ticks: {
            display: false
        },
            gridLines: {
              display: false,
              drawBorder: false,
              drawOnChartArea: false,
                
            }
            
        }
      ]
    }
    },
   
  })

  


  
});
